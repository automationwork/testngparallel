package com.hs.proj.model;

import com.hs.proj.enums.RoomType;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Booking {

    String email;
    String country;
    @ToString.Exclude String password;
    String dailyBudget;
    Boolean newsletter;
    RoomType roomType;
    String roomDescription;
}
