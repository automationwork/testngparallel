
package com.hs.proj.tests;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import com.hs.proj.configuration.Configuration;
import com.hs.proj.driver.DriverFactory;
import com.hs.proj.driver.BaseWebDriver;
import org.aeonbits.owner.ConfigCache;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

@Listeners({ExtentITestListenerClassAdapter.class, TestListener.class})
public abstract class BaseWeb {

    @BeforeSuite
    @Parameters("environment")
    public void setConfiguration(@Optional("dev") String environment) {
        String env = System.getenv("environment");
        ConfigFactory.setProperty("env", env == null ? environment : env);
    }

    @BeforeMethod
    @Parameters("browser")
    public void preCondition(@Optional("chrome") String browser) {
        Configuration configuration = ConfigCache.getOrCreate(Configuration.class);
        WebDriver driver = DriverFactory.createInstance(browser);
        BaseWebDriver.setDriver(driver);
        BaseWebDriver.getDriver().get(configuration.url());
    }

    @AfterMethod
    public void postCondition() {
        BaseWebDriver.quit();
    }
}
