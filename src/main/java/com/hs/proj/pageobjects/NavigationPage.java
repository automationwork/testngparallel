
package com.hs.proj.pageobjects;

import com.hs.proj.pageobjects.AbstractPageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationPage extends AbstractPageObject {

    @FindBy(name = "next")
    private WebElement next;

    @FindBy(name = "previous")
    private WebElement previous;

    @FindBy(name = "finish")
    private WebElement finish;

    public void next() {
        next.click();
    }

    public void previous() {
        previous.click();
    }

    public void finish() {
        finish.click();
    }
}
