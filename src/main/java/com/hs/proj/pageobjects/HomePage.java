package com.hs.proj.pageobjects;
import com.hs.proj.driver.BaseWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends Page {

    private String pageTitle = "The NHS website - NHS";
    private By enterSearchDetails = By.id("search-field");
    private By sumbmitSearch = By.className("nhsuk-search__submit");
    private By acceptCookiesLocator = By.id("nhsuk-cookie-banner__link_accept_analytics");


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void validateHomePageDetails() {
        waitForTitleToExist(pageTitle);
        waitForElementToBeVisibleBy(sumbmitSearch);
        waitForElementToBeVisibleBy(enterSearchDetails);
    }

    public void openHomePage() {
        BaseWebDriver.getDriver().get(new BaseContext().getBaseUrl());
    }

    public void readElements() {
        try {
            navigateToRootElement();
            navigateToElementBy(acceptCookiesLocator);
            click();
        } catch (Exception e) {
        }
        navigateToRootElement();
        navigateToElementBy(enterSearchDetails);
        type("data");

        navigateToRootElement();
        navigateToElementBy(sumbmitSearch);
        click();

        System.out.println();
    }

}
