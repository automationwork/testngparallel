package com.hs.proj.pageobjects;

import com.hs.proj.configuration.Configuration;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.aeonbits.owner.ConfigCache;

@Data
@Log4j2
public class BaseContext {

    private static ThreadLocal<BaseContext> baseContext = new ThreadLocal<>();

    private String target;
    private String environment;
    private String browserStackRunEnvironment;
    private String baseUrl;
    private String browser;
    private String headless;
    private String timestamp;
    private String gridUrl;
    private String gridPort;


    static Configuration configuration = ConfigCache.getOrCreate(Configuration.class,
            System.getProperties(),
            System.getenv());

    public BaseContext() {
        init();
    }

    private void init() {
        try {
            target = configuration.target();
            baseUrl = configuration.url();
            gridUrl = configuration.gridUrl();
            gridPort = configuration.gridPort();
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }

}




