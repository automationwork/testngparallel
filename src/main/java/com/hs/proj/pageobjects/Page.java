package com.hs.proj.pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Page {

    protected WebDriver driver;
    private WebElement element = null;
    private int explicitWaitTime = 1;
    private By headingLocator = By.id("service-name-heading-text");
    private By createdHeadingLocator = By.id("service-name-text");
    private By startPageHeadingLocator = By.id("main-heading");

    private By mynhssecondaryHeadingLOcator = By.className("heading-xlarge");

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public void tearDownDriver() {
        driver.quit();
    }

    public void goBack() {
        driver.navigate().back();
    }

    public void deleteCookies() {
        driver.manage().deleteAllCookies();
    }

    public void deleteCookies(String myCookie) {
        try {
            driver.manage().deleteCookieNamed(myCookie);
        } catch (NotFoundException e) {
            System.out.println("Cookie = " + myCookie + " NOT Found");
        }
    }

    public void navigateToRootElement() {
        element = driver.findElement(By.xpath("/html/body"));
    }

    public void navigateToParentElement() {
        element = element.findElement(By.xpath("./.."));
    }

    public void navigateToElementBy(By by) {
        element = element.findElement(by);
    }

    public WebElement getElementBy(By by) {
        element = element.findElement(by);
        return element;
    }

    public boolean isSelected() {
        return element.isSelected();
    }

    public boolean isEnabled() {
        return element.isEnabled();
    }

    public boolean isRadioButtonSelected(By By) {
        navigateToRootElement();
        return element.findElement(By).isSelected();
    }

    public List<WebElement> navigateToElementsBy(By by) {
        element = driver.findElement(By.xpath("/html/body"));
        return element.findElements(by);
    }

    public List<WebElement> navigateToElementsBy(By by1, By by2) {
        element = driver.findElement(By.xpath("/html/body"));
        return element.findElement(by1).findElements(by2);
    }

    public void click() {
        element.click();
    }

    public void performClickAction(WebElement elem) {
        Actions actions = new Actions(driver);
        actions.moveToElement(elem).click().perform();
    }

    public void editValueInDropDownField(String text) {
        int counter = 0;
        int actualLength = getLengthOfText();
        while (counter <= actualLength) {
            element.sendKeys(Keys.chord(Keys.DELETE));
            element.sendKeys(Keys.chord(Keys.BACK_SPACE));
            counter++;
        }
        element.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
        element.sendKeys(Keys.TAB);
    }

    public void clearValueInTextBox(By by) {
        element = driver.findElement(by);
        int counter = 0;
        int actualLength = getLengthOfText();
        while (counter <= actualLength) {
            element.sendKeys(Keys.chord(Keys.BACK_SPACE));
            counter++;
        }
    }


    private int getLengthOfText() {
        return element.getAttribute("value").length();
    }

    public void type(String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void waitForElementToBeVisibleBy(By by) {
        new WebDriverWait(driver, (long) explicitWaitTime).until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForTitleToExist(String title) {
        new WebDriverWait(driver, (long) explicitWaitTime).until(ExpectedConditions.titleIs(title));
    }

    public boolean getPresenceOfElement(By by) {
        boolean present = false;
        if (element.findElements(by).size() != 0) {
            present = true;
        }
        return present;
    }

    public String getElementText() {
        return element.getText();
    }

    public String getElementValue() {
        return element.getAttribute("value");
    }

    public String getHeading() {
        navigateToRootElement();
        navigateToElementBy(headingLocator);
        return getElementText();
    }

    public String getStartPageHeading() {
        navigateToRootElement();
        navigateToElementBy(startPageHeadingLocator);
        return getElementText();
    }


    public String getCreatedHeading() {
        navigateToRootElement();
        navigateToElementBy(createdHeadingLocator);
        return getElementText();
    }

    public String getDate(By monthLocator, By yearLocator) {
        navigateToRootElement();
        navigateToElementBy(monthLocator);
        String month = getElementText();
        navigateToRootElement();
        navigateToElementBy(yearLocator);
        String year = getElementText();
        return month + " " + year;
    }

    public String getDate(By dayLocator, By monthLocator, By yearLocator) {
        navigateToRootElement();
        navigateToElementBy(dayLocator);
        String day = getElementText();
        navigateToRootElement();
        navigateToElementBy(monthLocator);
        String month = getElementText();
        navigateToRootElement();
        navigateToElementBy(yearLocator);
        String year = getElementText();
        return day + " " + month + " " + year;
    }

    public boolean isElementSelected(By by) {
        navigateToElementBy(by);
        return element.isSelected();
    }

    public boolean isButtonEnabled() {
        return element.isEnabled();
    }


}
