package com.hs.proj.pageobjects;

import com.hs.proj.configuration.Configuration;
import com.hs.proj.driver.BaseWebDriver;
import org.aeonbits.owner.ConfigCache;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class AbstractPageObject {

    protected AbstractPageObject() {
        Configuration configuration = ConfigCache.getOrCreate(Configuration.class);
        int timeout = Integer.parseInt(configuration.timeout());
        
        PageFactory.initElements(new AjaxElementLocatorFactory(BaseWebDriver.getDriver(), timeout), this);
    }
}
