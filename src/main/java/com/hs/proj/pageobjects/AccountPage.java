package com.hs.proj.pageobjects;

import com.hs.proj.driver.BaseWebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class AccountPage {

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(name = "country")
    private WebElement country;

    @FindBy(name = "budget")
    private WebElement budget;

    @FindBy(css = ".check")
    private WebElement newsletter;

    public AccountPage() {
        BaseWebDriver.getDriver().switchTo().frame("result");
    }

    public void fillEmail(String email) {
        this.email.sendKeys(email);
    }

    public void fillPassword(String password) {
        this.password.sendKeys(password);
    }

    public void selectCountry(String country) {
        new Select(this.country).selectByVisibleText(country);
    }

    public void selectBudget(String value) {
        new Select(budget).selectByVisibleText(value);
    }

    public void clickNewsletter() {
        newsletter.click();
    }

    public void next() {
    }
}
