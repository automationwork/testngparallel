
package test;

import com.hs.proj.tests.BaseWeb;
import com.hs.proj.driver.BaseWebDriver;
import com.hs.proj.pageobjects.HomePage;
import org.testng.annotations.Test;

public class DummyWebTest extends BaseWeb {

    @Test(description = "Dummy Website test")
    public void bookARoom() {

        HomePage homepage = new HomePage(BaseWebDriver.getDriver());
        homepage.validateHomePageDetails();
        homepage.readElements();
    }
}
